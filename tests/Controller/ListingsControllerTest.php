<?php

namespace App\Tests\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ListingsControllerTest extends WebTestCase
{


    public function testCreateAndAddTicketsAction()
    {
        $client = static::createClient();
        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $crawler = $client->request('GET', '/listing/create');
        $form = $crawler->selectButton('Save')->form();
        $user = $em->getRepository('App:User')->findOneBy(['name' => 'Frank']);
        // Submitting empty form
        $this->submitFormBlank($client, $form, $user);
        // Negative amount for price or invalid characters
        $this->submitFormWithBadData($client, $form, $user);

        // Test Create a Listing
        $listingId = $this->submitValidForm($client, $form, $user);

        $crawler = $client->followRedirect();

        $addTicketForm = $crawler->selectButton('Add')->form();
        // Test bad data for barcode
        $this->addBarcodeWithBadData($client, $addTicketForm);

        // Add a real barcode
        $ticketId = $this->addBarcode($client, $addTicketForm);

        // Remove and reset tested data
        $this->resetData($client, $listingId, $ticketId);
    }

    protected function submitFormBlank($client, $form, $user)
    {
        $form['listing_form[user]'] = $user->getId();
        $crawler = $client->submit($form);

        $this->assertCount(
            2,
            $crawler->filter('form .form-error-message:contains("This value should not be blank.")')
        );
    }

    protected function submitFormWithBadData($client, $form, $user)
    {
        $form['listing_form[user]'] = $user->getId();
        $form['listing_form[price]'] = -100;
        $form['listing_form[description]'] = 'Test';
        $crawler = $client->submit($form);

        $this->assertCount(
            1,
            $crawler->filter('form .form-error-message:contains("Negative price? Why?")')
        );

        $form['listing_form[user]'] = $user->getId();
        $form['listing_form[price]'] = 'blah';
        $form['listing_form[description]'] = 'Test';
        $crawler = $client->submit($form);

        $this->assertCount(
            1,
            $crawler->filter('form .form-error-message:contains("This value should be a valid number.")')
        );
    }

    protected function submitValidForm ($client, $form, $user)
    {
        $form['listing_form[user]'] = $user->getId();
        $form['listing_form[price]'] = 100;
        $form['listing_form[description]'] = 'Test';
        $crawler = $client->submit($form);
        $redirectedUrl = $client->getResponse()->headers->get('location');
        $this->assertRegExp('/\/\blisting.\d+\b\/ticket\/add/', $redirectedUrl);

        $createdListingId = explode('/', $redirectedUrl)[2];
        return $createdListingId;
    }

    protected function addBarcodeWithBadData($client, $form)
    {
        // Empty form
        $crawler = $client->submit($form);
        $this->assertCount(
            1,
            $crawler->filter('form .form-error-message:contains("This value should not be blank.")')
        );

        // Form with invalid barcode
        $form['listing_ticket_form[barcode]'] = 'blah';
        $crawler = $client->submit($form);
        $this->assertCount(
            1,
            $crawler->filter('form .form-error-message:contains("The provided barcode is invalid!")')
        );
    }

    protected function addBarcode($client, $form)
    {
        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $testBarcode = 'EAN-13:111111111';
        $ticket = $em->getRepository('App:Ticket')->findTicketByBarcode($testBarcode);
        $ticketListingIdBeforeSubmit = $ticket->getListing();
        $form['listing_ticket_form[barcode]'] = $testBarcode;
        $crawler = $client->submit($form);

        // check if ticket is assigned to a listing
        if ($ticketListingIdBeforeSubmit !== null) {
            $this->assertCount(
                1,
                $crawler->filter('form .form-error-message:contains("This ticket is already assigned to a listing!")')
            );
        } else {
            $redirectedUrl = $client->getResponse()->headers->get('location');
            $this->assertRegExp('/\/\blisting.\d+\b\/tickets/', $redirectedUrl);
            return $ticket->getId();
        }

    }

    protected function resetData($client, $listingId, $ticketId)
    {
        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        if ($ticketId) {
            $ticket = $em->getRepository('App:Ticket')->find($ticketId);
            if ($ticket) {
                $ticket->setListing(null);
                $em->persist($ticket);
                $em->flush();
            }
        }
        if ($listingId) {
            $listing = $em->getRepository('App:Listing')->find($listingId);
            if ($listing) {
                $em->remove($listing);
                $em->flush();
            }
        }
    }
}
