<?php


namespace App\Validator\Constraints;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TicketAccessValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($values, Constraint $constraint)
    {
        $barcode = $this->em
            ->getRepository('App:Barcode')
            ->findOneBy(['barcode' => $values['barcode']]);

        if (!$barcode) {
            $this->context->buildViolation($constraint->barcodeMessage)
                ->addViolation();
            return;
        }

        $ticket = $barcode->getTicket();
        if (!$ticket) {
            $this->context->buildViolation($constraint->ticketMessage)
                ->addViolation();
            return;
        }

        if ($ticket->getListing() !== null) {
            $this->context->buildViolation($constraint->listingMessage)
                ->addViolation();
            return;
        }

        $listing = $this->em->getRepository('App:Listing')->find($values['listing']);
        if (!$listing) {
            $this->context->buildViolation($constraint->unknownListingMessage)
                ->addViolation();
            return;
        }

        if ($ticket->getUser() && $listing->getUser() !== $ticket->getUser()) {
            $this->context->buildViolation($constraint->noTicketAccessMessage)
                ->addViolation();
            return;
        }

    }

}