<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class TicketAccess extends Constraint
{
    public $barcodeMessage = 'The provided barcode is invalid!';
    public $ticketMessage = 'No ticket found with the provided barcode!';
    public $listingMessage = 'This ticket is already assigned to a listing!';
    public $unknownListingMessage = 'Unknown Listing!!!(This error shouldn\'t be shown at all, unless someone\'s sneaking around!)';
    public $noTicketAccessMessage = 'You are not the buyer of this ticket! You can\'t add it to a listing! Please use another barcode';

    /**
     * @Annotation
     */
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}