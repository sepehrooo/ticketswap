<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180808071728 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barcode ADD listing_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE barcode ADD CONSTRAINT FK_97AE0266D4619D1A FOREIGN KEY (listing_id) REFERENCES listing (id)');
        $this->addSql('CREATE INDEX IDX_97AE0266D4619D1A ON barcode (listing_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barcode DROP FOREIGN KEY FK_97AE0266D4619D1A');
        $this->addSql('DROP INDEX IDX_97AE0266D4619D1A ON barcode');
        $this->addSql('ALTER TABLE barcode DROP listing_id');
    }
}
