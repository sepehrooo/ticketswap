<?php

namespace App\Repository;

use App\Entity\ListingBarcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ListingBarcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListingBarcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListingBarcode[]    findAll()
 * @method ListingBarcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListingBarcodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ListingBarcode::class);
    }

//    /**
//     * @return ListingBarcode[] Returns an array of ListingBarcode objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListingBarcode
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
