<?php


namespace App\Controller;


use App\Entity\Listing;
use App\Form\ListingFormType;
use App\Form\ListingTicketFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ListingsController extends AbstractController
{
    /**
     * @Route("/listing/create", name="listing_create")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ListingFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listing = $form->getData();
            $em->persist($listing);
            $em->flush();

            $this->addFlash('success', 'Listing created!');
            return $this->redirectToRoute('add_ticket_listing', ['id' => $listing->getId()]);
        }

        return $this->render('listing/create.html.twig', [
            'listingForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/", name="listing_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $listings = $em->getRepository('App:Listing')->findAll();
        $listingsArr = [];
        foreach ($listings as $listing) {
            $listingsArr[] = [
                'id' => $listing->getId(),
                'price' => $listing->getPrice(),
                'description' => $listing->getDescription(),
                'username' => $listing->getUser()->getName()
            ];
        }
        return $this->render('listing/list.html.twig', [
            'listings' =>  $listingsArr
        ]);
    }

    /**
     * @Route("/listing/{id}/ticket/add", name="add_ticket_listing")
     */
    public function addTicketToListingAction(Request $request, Listing $listing)
    {
        $em = $this->getDoctrine()->getManager();
        $ticketRepository = $em->getRepository('App:Ticket');
        $form = $this->createForm(ListingTicketFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $ticket = $ticketRepository->findTicketByBarcode($formData['barcode']);
            $ticket->setListing($listing);
            $em->persist($ticket);
            $em->flush();

            $this->addFlash('success', 'Ticket added to the listing!');
            return $this->redirectToRoute('listing_tickets', ['id' => $listing->getId()]);
        }

        return $this->render('listing/add-ticket.html.twig', [
            'listingTicketForm' => $form->createView(),
            'listing' => $listing
        ]);
    }

    /**
     * @Route("/listing/{id}/tickets", name="listing_tickets")
     */
    public function listingTicketsAction(Listing $listing)
    {
        $tickets = $listing->getTickets();
        $ticketsArr = [];
        foreach ($tickets as $ticket) {
            $ticketsArr[] = [
                'id' => $ticket->getId(),
                'username' => null !== $ticket->getUser() ? $ticket->getUser()->getName() : null,
                'boughtAtDate' => $ticket->getBoughtAtDate()
            ];
        }
        return $this->render('listing/list-tickets.html.twig', [
            'tickets' =>  $ticketsArr,
            'listing' => $listing
        ]);
    }
}