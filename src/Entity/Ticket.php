<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $listingId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Barcode", mappedBy="ticket")
     */
    private $barcodes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Listing", inversedBy="tickets")
     */
    private $listing;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tickets")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $boughtAtDate;

    public function __construct()
    {
        $this->barcodes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getListingId(): ?int
    {
        return $this->listingId;
    }

    public function setListingId(?int $listingId): self
    {
        $this->listingId = $listingId;

        return $this;
    }

    /**
     * @return Collection|Barcode[]
     */
    public function getBarcodes(): Collection
    {
        return $this->barcodes;
    }

    public function addBarcode(Barcode $barcode): self
    {
        if (!$this->barcodes->contains($barcode)) {
            $this->barcodes[] = $barcode;
            $barcode->setTicket($this);
        }

        return $this;
    }

    public function removeBarcode(Barcode $barcode): self
    {
        if ($this->barcodes->contains($barcode)) {
            $this->barcodes->removeElement($barcode);
            // set the owning side to null (unless already changed)
            if ($barcode->getTicket() === $this) {
                $barcode->setTicket(null);
            }
        }

        return $this;
    }

    public function getListing(): ?Listing
    {
        return $this->listing;
    }

    public function setListing(?Listing $listing): self
    {
        $this->listing = $listing;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBoughtAtDate(): ?\DateTimeInterface
    {
        return $this->boughtAtDate;
    }

    public function setBoughtAtDate(?\DateTimeInterface $boughtAtDate): self
    {
        $this->boughtAtDate = $boughtAtDate;

        return $this;
    }
}
