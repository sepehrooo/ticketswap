<?php

namespace App\DataFixtures;

use App\Entity\Barcode;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BarcodeFixture extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return array(
            TicketFixture::class,
        );
    }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $barcodes = [
            'EAN-13:111111111',
            'EAN-13:222222222',
            'EAN-13:333333333',
            'EAN-13:444444444',
            'EAN-13:555555555',
            'EAN-13:666666666',
            'EAN-13:777777777',
            'EAN-13:888888888'
        ];
        $tickets = [
            $this->getReference('Ticket_1'),
            $this->getReference('Ticket_1'),
            $this->getReference('Ticket_2'),
            $this->getReference('Ticket_3'),
            $this->getReference('Ticket_4'),
            $this->getReference('Ticket_5'),
            $this->getReference('Ticket_5'),
            $this->getReference('Ticket_5')
        ];
        for ($i=0; $i < 8; $i++) {
            $barcode = new Barcode();
            $barcode->setBarcode($barcodes[$i]);
            $barcode->setTicket($tickets[$i]);

            $manager->persist($barcode);
        }

        $manager->flush();
    }
}
