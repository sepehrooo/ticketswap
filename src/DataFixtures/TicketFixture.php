<?php

namespace App\DataFixtures;

use App\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TicketFixture extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return array(
            UserFixture::class,
        );
    }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $users = [
            null,
            null,
            $this->getReference('User_1'),
            $this->getReference('User_3'),
            $this->getReference('User_2'),
            $this->getReference('User_1'),
            $this->getReference('User_2'),
        ];
        $boughtDates = [
            null,
            null,
            new \DateTime('2016-01-20 10:00:00'),
            new \DateTime('2016-01-20 16:00:00'),
            new \DateTime('2016-02-20 10:00:00'),
            new \DateTime('2016-03-20 12:00:00'),
            new \DateTime('2016-03-20 12:05:00'),
        ];
        for ($i=0; $i < 7; $i++) {
            $ticket = new Ticket();
            $ticket->setUser($users[$i]);
            $ticket->setBoughtAtDate($boughtDates[$i]);
            $manager->persist($ticket);

            $this->setReference('Ticket_' . ($i + 1), $ticket);
        }

        $manager->flush();
    }
}
