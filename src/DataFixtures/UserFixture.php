<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $names = ['Frank', 'Ruud', 'Hans', 'Joe'];
        for ($i=0; $i < 4; $i++) {
            $user = new User();
            $user->setName($names[$i]);
            $manager->persist($user);
            $this->addReference('User_' . ($i + 1), $user);
        }
        $manager->flush();
    }
}
